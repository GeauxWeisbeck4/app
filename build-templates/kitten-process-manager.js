////////////////////////////////////////////////////////////
//
// Kitten Process Manager
//
// Basic process manager to restart Kitten in the absence
// of systemd during development.
//
// Copyright ⓒ 2022-present, Aral Balkan (mail@ar.al)
// Small Technology Foundation (https://small-tech.org)
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import cluster from 'node:cluster'

if (cluster.isPrimary) {
  // We are the primary process (the process manager).
  let kittenProcess = null

  function createNewKittenProcess () {
    kittenProcess = cluster.fork()

    kittenProcess.on('exit', (code, _signal) => {
      if (code === 99 /* restart request */) {
        console.info("\n╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌\n")
        createNewKittenProcess()
      } else {
        process.exit(code)
      }
    })
  }
  
  // Launch first Kitten process.
  createNewKittenProcess()
} else {
  // We are not the primary process (the process manager),
  // therefore, we should be a Kitten instance. Meow, and all that!
  await import('./kitten-bundle.js')
}
