////////////////////////////////////////////////////////////////////////////////
//
// Command-line interface smoke tests.
//
////////////////////////////////////////////////////////////////////////////////

import test from '@small-tech/tape-with-promises'

import CLI from '../src/cli/index.js'

import serveCommand from '../src/cli/commands/serve'
import deployCommand from '../src/cli/commands/deploy'
import statusCommand from '../src/cli/commands/status'
import logsCommand from '../src/cli/commands/logs'
import startCommand from '../src/cli/commands/start'
import stopCommand from '../src/cli/commands/stop'
import enableCommand from '../src/cli/commands/enable'
import disableCommand from '../src/cli/commands/disable'
import uninstallCommand from '../src/cli/commands/uninstall'

import packageInfo from '../package.json' assert {type: 'json'}

const consoleLogOutput = []
const consoleInfoOutput = []
const consoleWarnOutput = []
const consoleErrorOutput = []

const processExitCodes = []

// Monkey patch process.exit so commands don’t actually exit.
const originalProcessExit = process.exit
process.exit = code => { processExitCodes.push(code === undefined ? 0 : code) }

// Monkey patch console methods so we can check the version being reported.
const originalConsoleLog = console.log
const originalConsoleInfo = console.info
const originalConsoleWarn = console.warn
const originalConsoleError = console.error

console.log = (...output) => { consoleLogOutput.push(output); originalConsoleLog.apply(console, output) }
console.info = (...output) => { consoleInfoOutput.push(output); } // Only we use info in the app so no need to forward and add noise to test output.
console.warn = (...output) => { consoleWarnOutput.push(output); originalConsoleWarn.apply(console, output) }
console.error = (...output) => { consoleErrorOutput.push(output); originalConsoleError.apply(console, output) }

test.onFinish(() => {
  // Revert the monkey patches.
  console.log = originalConsoleLog
  console.info = originalConsoleInfo
  console.warn = originalConsoleWarn
  console.error = originalConsoleError
  process.exit = originalProcessExit
})

let commands = (new CLI()).commands

test('default', t => {
  t.equals(commands.default, 'serve', 'Default command is serve.')
  t.end()
})

test('version', t => {
  t.plan(4)
  t.equals(commands.ver, packageInfo.version, 'CLI version matches package.json version.')

  // Run version command.
  commands.parse([null, null, '--version'])

  t.equals(processExitCodes[0], 0, 'Version command exits without error')
  t.equals(consoleInfoOutput[0][0], '😸 Kitten', 'Version output displays app name')
  t.equals(consoleInfoOutput[0][1], `v${packageInfo.version}`, 'Version is correct')
})

test('command handlers exist', t => {
  t.equals(commands.tree.serve.handler, serveCommand, 'Serve command handler exists.')
  t.equals(commands.tree.deploy.handler, deployCommand, 'Deploy command handler exists.')
  t.equals(commands.tree.status.handler, statusCommand, 'Status command handler exists.')
  t.equals(commands.tree.logs.handler, logsCommand, 'Logs command handler exists.')
  t.equals(commands.tree.start.handler, startCommand, 'Start command handler exists.')
  t.equals(commands.tree.stop.handler, stopCommand, 'Stop command handler exists.')
  t.equals(commands.tree.enable.handler, enableCommand, 'Enable command handler exists.')
  t.equals(commands.tree.disable.handler, disableCommand, 'Disable command handler exists.')
  t.equals(commands.tree.uninstall.handler, uninstallCommand, 'Uninstall command handler exists.')
  t.end()
})

// TODO: Add specific command tests.

test('serve handler', t => {
  // TODO
  t.end()
})

test('enable handler', t => {
  // TODO
  t.end()
})
