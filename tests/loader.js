import test from '@small-tech/tape-with-promises'
import { resolve } from '../src/processes/loader'
import os from 'os'

const homeDirectory = os.homedir()

test('resolve', async t => {
  // The context conditions passed during resolution.
  const conditions = [ 'node', 'import', 'node-addons' ]

  // Paths/URLs used in the tests.
  // Gathered from a run of Domain (https://github.com/small-tech/domain).
  const kittenBaseUrl = `file://${homeDirectory}/.local/share/small-tech.org/kitten/app`
  const appBaseUrl = `file://${homeDirectory}/Projects/small-web/domain`
  const kittenBundleUrl = `${kittenBaseUrl}/kitten-bundle.js`
  const adminIndexPagePath = `${homeDirectory}/Projects/small-web/domain/admin/index.page`
  const adminIndexPageFileUrl = `file://${adminIndexPagePath}`
  const placesComponentFileUrl = `${appBaseUrl}/admin/places/Index.component`
  const paymentBaseFileUrl = `file://${homeDirectory}/Projects/small-web/domain/admin/setup/payment`

  const defaultResolve = 'expect-default-resolve'

  const resolutions = [
    // Kitten bundle itself.
    {
      specifier: kittenBundleUrl,
      parentURL: undefined,
      resolvesTo: defaultResolve
    },

    // An internal node package.
    {
      specifier: 'module',
      parentURL: kittenBundleUrl,
      resolvesTo: defaultResolve
    },

    // A page from within the app/site being served.
    {
      specifier: adminIndexPagePath,
      parentURL: kittenBundleUrl,
      resolvesTo: `file://${adminIndexPagePath}`
    },

    // An internal Node package imported by the app/site being served.
    {
      specifier: 'node:buffer',
      parentURL: adminIndexPageFileUrl,
      resolvesTo: defaultResolve
    },

    // A third-party regular Node module imported from a component in the app/site.
    {
      specifier: '@small-tech/spinners',
      parentURL: placesComponentFileUrl,
      resolvesTo: defaultResolve
    },

    // A component imported from another component in the app/site using a relative path (component extension).
    {
      specifier: './Tokens.component',
      parentURL: `${paymentBaseFileUrl}/Index.component`,
      resolvesTo: `${paymentBaseFileUrl}/Tokens.component`
    },

    // Loading one module from another with a relative specifier in the app/site.
    {
      specifier: './Util.js',
      parentURL: `${appBaseUrl}/library/JSDB/DataProxy.js`,
      resolvesTo: defaultResolve
    },

    // A regular module imported from a component using a relative app in the app/site.
    {
      specifier: '../../library/Constants',
      parentUrl: `${appBaseUrl}/admin/setup/PSL.component`,
      resolvesTo: defaultResolve
    }
  ]

  t.plan(resolutions.length)

  for (const resolution of resolutions) {
    let defaultResolveFunction
    if (resolution.resolvesTo === defaultResolve) {
      defaultResolveFunction = () => {
        t.pass(`defaultResolve called as expected for specifier ${resolution.specifier}`)
      }
    } else {
      defaultResolveFunction = () => {
        t.fail(`defaultResolve unexpectedly called for specifier ${resolution.specifier}`)
      }
    }
    const resolved = await resolve(
      resolution.specifier,
      { 
        parentURL: resolution.parentURL,
        conditions
      }, // (context)
      defaultResolveFunction
    )

    if (resolution.resolvesTo !== defaultResolve) {
      t.ok(resolved.url.startsWith(resolution.resolvesTo), `Custom resolution as expected for specifier ${resolution.specifier}`)
    }
  }
})
