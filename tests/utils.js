import test from '@small-tech/tape-with-promises'
import { withoutWhitespace } from './helpers/index.js'

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'
import assert from 'node:assert/strict'

import * as utils from '../src/Utils.js'

test('basepath', t => {
  const relativePathToEmptyProject = 'tests/fixtures/emptyProject'
  const absolutePathToEmptyProject = path.resolve(relativePathToEmptyProject)

  const relativePathToProjectWithSrcFolder = 'tests/fixtures/emptyProjectWithSrcFolder'
  const absolutePathToProjectWithSrcFolder = path.resolve(relativePathToProjectWithSrcFolder)
  
  // Since these are empty folders, they will not be held in the 
  // git repository. So we ensure they exist and create them if they do not
  // our test doesn’t fail erroneously.
  if (!fs.existsSync(absolutePathToEmptyProject)) {
    fs.mkdirSync(absolutePathToEmptyProject, {recursive: true})
  }
  
  if (!fs.existsSync(absolutePathToProjectWithSrcFolder)) {
    fs.mkdirSync(path.join(absolutePathToProjectWithSrcFolder, 'src'), {recursive: true})
  }
  
  const relativeNonExistentPath = 'this-path-does-not-exist'
  const absoluteNonExistentPath = path.resolve(relativeNonExistentPath)

  const defaultWorkingDirectory = '.'
  const basePath = utils.setBasePath(defaultWorkingDirectory, relativePathToEmptyProject)
  const basePathWithSourceFolder = utils.setBasePath(defaultWorkingDirectory, relativePathToProjectWithSrcFolder)

  t.equal(basePath, absolutePathToEmptyProject, 'base path to empty project is correct')
  t.equal(basePathWithSourceFolder, path.join(absolutePathToProjectWithSrcFolder, 'src'), 'base path to project with src folder is correct')
  t.throws(
    () => utils.setBasePath(defaultWorkingDirectory, relativeNonExistentPath),
    error => {
      assert(error.message.includes(`${absoluteNonExistentPath} – path does not exist`))
      return true
    },
    'Non-existent paths throw.'
  )
  
  // TODO: Also test non-default working directory.
  // TODO: Also test that process.env.basePath is set.

  t.end()
})

test ('class name from route', t => {
  t.equal(
    utils.classNameFromRoutePattern('/some_route/with/underscores-and-hyphens:and-a-parameter/or:two'), 'SomeRouteWithUnderscoresAndHyphensAndAParameterOrTwoPage'
  )
  t.equal(utils.classNameFromRoutePattern('/'), 'IndexPage')
  t.end()
})

test ('routePatternFromFilePath', t => {
  const basePath = process.env.basePath
  
  const supportedExtensions = ['get', 'head', 'patch', 'options', 'connect', 'delete', 'trace', 'post', 'put', 'page', 'socket']

  // Some manual tests of actual routes in the Domain app (https://github.com/small-tech/domain).
  const expectations = [
    [path.join(basePath, 'index_[property1]_and_[property2].page'), '/:property1/and/:property2'],
    [path.join(basePath, 'index_with_[property1]_and_[property2].page'), '/with/:property1/and/:property2'],
    [path.join(basePath, 'admin/index_[password].socket'), '/admin/:password'],
    [path.join(basePath, 'domain/available_[domain].get'), '/domain/available/:domain'],
    [path.join(basePath, 'private_[token]_[domain].get'), '/private/:token/:domain']
  ]

  for (const supportedExtension of supportedExtensions) {
    expectations.push([
      path.join(basePath, 'a', 'route-with', `this_[property].${supportedExtension}`),
      `/a/route-with/this/:property`
    ])
  }

  for (const expectation of expectations) {
    t.equal(utils.routePatternFromFilePath(expectation[0]), expectation[1], expectation[0])
  }

  t.end()
})

test ('kittenAppPath', t => {
  // Note: this doesn’t test the app path when run from the Kitten bundle or 
  // the Kitten launch script from source. It only tests it when run from this
  // file in the unit tests.
  t.equal(utils.kittenAppPath, path.resolve('.') + '/')
  t.end()
})
