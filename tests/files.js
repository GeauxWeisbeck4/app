import test from '@small-tech/tape-with-promises'

import path from 'path'
import { fileURLToPath, URL } from 'url'
import process from 'process'

const __dirname = fileURLToPath(new URL('.', import.meta.url))
const fixturesPath = path.join(__dirname, 'fixtures')
const testFixturesDirectory = path.join(process.cwd(), 'tests', 'fixtures')

import Files from '../src/Files'

function sort(filesByExtension) {
  // Chokidar’s file system scan does not always return the files in the same order.
  // So we perform a sort before comparing them.
  Object.keys(filesByExtension).forEach(extension => {
    filesByExtension[extension].sort()
  })
  return filesByExtension
}

// test('files', async t => {
//   process.env.basePath = path.join(fixturesPath, 'files')

//   const files = new Files()
//   const filesByExtensionCategoryType = await files.initialise()
  
//   // TODO
// })

test('chokidar error handling', async t => {
  process.env.basePath = '/'
  const files = new Files()
  await t.rejects(async () => await files.initialise(), /EACCES/, 'attempting to watch root should throw EACCES')
  files.close()
})
