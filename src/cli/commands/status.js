////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): status command.
//
// Display Kitten systemd service status.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import childProcess from 'node:child_process'
import chalk from 'chalk'

// TODO: Pull this out to the utility file.
function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

export default function stop (_options) {
  try {
    childProcess.spawn(
      'systemctl', 
      ['--user', '--no-pager', '--full', 'status', 'kitten'],
      {env: process.env, stdio: 'inherit'}
    )
  } catch (error) {
    exitWithError(`Could not get Kitten service status: ${error}.`)
  }

  process.exit()
}
