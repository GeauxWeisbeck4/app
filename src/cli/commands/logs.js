////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): logs command.
//
// Follow Kitten systemd service logs.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import childProcess from 'node:child_process'
import chalk from 'chalk'

// TODO: Pull this out to the utility file.
function exitWithError (code, message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(2)
}

export default function logs (options) {
  console.info(`📜 Following logs (press Ctrl+C to exit).\n`)
  try {
    childProcess.spawn(
      'journalctl', 
      ['--since', options['since'] || '1 hour ago', '--no-pager', '--follow', '--user', '--unit', 'kitten'],
      {env: process.env, stdio: 'inherit'}
    )
  } catch (error) {
    exitWithError(`Could not show Kitten service logs: ${error}.`)
  }
}
