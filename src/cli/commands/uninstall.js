////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): uninstall command.
//
// Copyright ⓒ 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import process from 'process'
import fs from 'fs'

import chalk from 'chalk'
import yesNo from 'yesno'

import stop from './stop'
import disable from './disable'

import paths from '../../lib/paths'
import serviceStatus from '../lib/service-status'
import WarningBox from '../lib/WarningBox'

console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}

const ok = chalk.green
const emphasised = chalk.bold
const warning = chalk.bold.yellow
const danger = chalk.bold.red

export default async function uninstall () {
  function abort () {
    console.info("\nAborted.")
    process.exit()
  }
  
  const { isActive: serviceIsActive, isEnabled: serviceIsEnabled } = serviceStatus()

  const box = new WarningBox()
  box.line(`${warning('WARNING!')} ${ok('About to uninstall Kitten.')}`)

  // Check if the server is active/enabled and add a note about that to the warning box.
  if (serviceIsActive && serviceIsEnabled) {
    box.emptyLine()
    box.line(`• ${warning('Kitten service is active and enabled.')}`)
    box.line('  It will be stopped and disabled.')
  } else if (serviceIsActive) {
    box.emptyLine()
    box.line(`• ${warning('Kitten service is active.')}`)
    box.line('  It will be stopped.')
  } else if (serviceIsEnabled) {
    box.emptyLine()
    box.line(`• ${warning('Kitten service is enabled.')}`)
    box.line('  It will be disabled.')
  }

  box.print()
  
  // TODO: Currently, we are not deleting databases to be on the safe side. But 
  // someone might want a full uninstall, so we should really be showing any
  // databases that exist and asking the person whether they want to delete them
  // or not as part of the uninstall process.
  
  const confirmation = await yesNo({
    question: `${danger('🙀 This will delete Kitten.')}\n\nType ${emphasised('yes')} to continue if you’re sure…`,
    yesValues: [ 'yes' ],
    defaultValue: null, // This is too important to leave to an errant return.
    invalid: abort
  })
  
  if (!confirmation) {
    abort()
  }

  console.info("\nUninstalling Kitten…")
  
  // fs.rmSync options.
  const force = {force: true}
  const forceRecursive = {force: true, recursive: true}
  
  if (serviceIsActive) {
    stop()
  }
  
  if (serviceIsEnabled) {
    disable()

    console.info('  • Removing Kitten systemd unit file.')
    fs.rmSync(paths.KITTEN_SYSTEMD_UNIT_PATH, force)
  }
  
  console.info('  • Removing Kitten app directory (your databases are untouched).')
  fs.rmSync(paths.KITTEN_APP_DIRECTORY_PATH, forceRecursive)
  
  console.info('  • Removing Kitten runtime.')
  fs.rmSync(paths.KITTEN_RUNTIME_DIRECTORY, forceRecursive)
  
  console.info('  • Removing Kitten temporary folder.')
  fs.rmSync(paths.KITTEN_TEMP_DIRECTORY, forceRecursive)
  
  console.info('  • Removing Kitten binary (symlink).')
  fs.rmSync(paths.KITTEN_BINARY_PATH, force)

  console.info("\nDone.")
  process.exit()
}
