////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): stop command.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import childProcess from 'node:child_process'
import chalk from 'chalk'
import serviceStatus from '../lib/service-status'

// TODO: Pull this out to the utility file.
function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

export default function stop (_options) {
  const { isActive } = serviceStatus()
  
  if (!isActive) {
    exitWithError('Kitten service is not active. Nothing to stop.')
  }

  console.info('  • Stopping Kitten service.')

  const options = {env: process.env, stdio: 'pipe'}
  try {
    childProcess.execSync('systemctl --user stop kitten', options)
  } catch (error) {
    exitWithError(`Could not stop the Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
