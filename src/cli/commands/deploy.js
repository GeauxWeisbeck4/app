////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): deploy command.
//
// Deploys an app from a Git repository as a
// systemd --user service.
//
// Copyright ⓒ 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'
import childProcess from 'node:child_process'

import chalk from 'chalk'
import simpleGit from 'simple-git'

import paths from '../../lib/paths'
import ensure from '../lib/ensure'

const git = simpleGit()

/**
* @param {string} gitHttpsCloneUrl - git clone url (HTTPS)
*/
export default async function deploy (gitHttpsCloneUrl, options) {
  console.info('Deploying service.')
  console.info()

  await ensure.canEnableService()
  
  if (!gitHttpsCloneUrl.startsWith('https://') || !gitHttpsCloneUrl.endsWith('.git')) {
    if (gitHttpsCloneUrl.startsWith('git@')) {
      console.error(`${chalk.bold.red('Error:')} please provide an HTTPS protocol git clone URL, not SSH.`)
      console.error(chalk.blue('(It should start with https://, not git@)'))
    } else {
      console.error(`${chalk.bold.red('Error:')} invalid git clone URL ${chalk.blue('(must begin with https:// and end with .git).')}`)
    }
    console.error(chalk.italic('\nTo avoid errors, copy the HTTPS URL from your repository host’s web interface to ensure it’s correct.'))
    process.exit(1)
  }

  // Create options for serve command to write into systemd unit.
  const hostname = os.hostname()
  const domain = options.domain || hostname
  const domainOption = `--domain=${domain}`
  const aliasesOption = options.aliases ? `--aliases=${options.aliases}` : ''
    
  // Generate working copy path based on clone URL.
  const deploymentDirectoryName = gitHttpsCloneUrl.replace('https://', '').replace('.git', '').replace(/\//g, '.')
  const deploymentPath = path.join(paths.KITTEN_DEPLOYMENTS_DIRECTORY, deploymentDirectoryName)
  
  // Ensure main deployments directory exists.
  fs.mkdirSync(paths.KITTEN_DEPLOYMENTS_DIRECTORY, { recursive: true })
  
  // Make sure we start with a fresh deployment directory in case this app was deployed previously.
  fs.rmSync(deploymentPath, {recursive: true, force: true})

  console.info(`  • Cloning repository ${chalk.italic(`(${gitHttpsCloneUrl})`)}`)

  await git.clone(gitHttpsCloneUrl, deploymentPath)

  console.info(`  • Writing systemd service unit ${chalk.italic(`(${paths.KITTEN_SYSTEMD_UNIT_PATH})`)}`)

  const unit = `
  [Unit]
  Description=Kitten Service
  Documentation=https://codeberg.org/kitten/app/src/README.md

  [Service]
  Type=simple
  RestartSec=1s
  Restart=on-failure
  WorkingDirectory=${deploymentPath}
  Environment="PRODUCTION=true"
  ExecStart=${paths.KITTEN_BINARY_PATH} ${deploymentPath} ${domainOption} ${aliasesOption}

  [Install]
  WantedBy=default.target
  `

  fs.writeFileSync(paths.KITTEN_SYSTEMD_UNIT_PATH, unit, {encoding: 'utf-8'})

  console.info('  • Starting Kitten service.')

  childProcess.execSync('systemctl start --user kitten')

  console.info('  • Installing Kitten service to launch automatically at boot, restart on errors, etc.')

  childProcess.execSync('systemctl enable --user kitten')

  console.info("\nDone.")
  process.exit()
}
