////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): disable command.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import fs from 'node:fs'
import childProcess from 'node:child_process'

import chalk from 'chalk'

import paths from '../../lib/paths'
import serviceStatus from '../lib/service-status'

// TODO: Pull this out to the utility file.
function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

export default function disable (_options) {
  const { isEnabled } = serviceStatus()
  
  if (!isEnabled) {
    exitWithError('Kitten service is not enabled. Nothing to disable.')
  }

  console.info('  • Disabling Kitten service.')

  const options = {env: process.env, stdio: 'pipe'}
  try {
    childProcess.execSync('systemctl --user disable kitten', options)
  } catch (error) {
    exitWithError(`Could not disable Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
