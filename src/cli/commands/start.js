////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): start command.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import childProcess from 'node:child_process'
import chalk from 'chalk'
import serviceStatus from '../lib/service-status'

// TODO: Pull this out to the utility file.
function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

export default function stop (_options) {
  const { isActive } = serviceStatus()
  
  if (isActive) {
    exitWithError('Kitten service is already active. Nothing to start.')
  }

  console.info('  • Starting Kitten service.')

  const options = {env: process.env, stdio: 'pipe'}
  try {
    childProcess.execSync('systemctl --user start kitten', options)
  } catch (error) {
    exitWithError(`Could not start Kitten service: ${error}.`)
  }
  
  console.info("\nDone.")
  process.exit()
}
