////////////////////////////////////////////////////////////
//
// Kitten command-line interface (CLI): serve command.
//
// Copyright ⓒ 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import { setBasePath } from '../../Utils.js'
import Server from '../../Server.js'

export default async function serve (pathToServe = '.', options) {
  const workingDirectory = options['working-directory']

  let basePath
  try {
    basePath = setBasePath(workingDirectory, pathToServe)
  } catch (error) {
    console.error(`❌ Error: cannot serve ${error.message}`)
    process.exit(1)
  }
  
  const server = new Server(options)
  await server.initialise()
}
