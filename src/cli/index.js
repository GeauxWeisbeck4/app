////////////////////////////////////////////////////////////////////////////////
//
// 😸️ Kitten Command Line Interface (CLI).
//
// Copyright © 2022-present Aral Balkan, Small Technology Foundation.
// Licensed under GNU AGPL version 3.0.
//
// https://kitten.small-web.org
//
////////////////////////////////////////////////////////////////////////////////

import sade from 'sade'
import packageInfo from '../../package.json' assert {type: 'json'}

import header from './lib/header'

import serveCommand from './commands/serve'
import deployCommand from './commands/deploy'
import statusCommand from './commands/status'
import logsCommand from './commands/logs'
import startCommand from './commands/start'
import stopCommand from './commands/stop'
import enableCommand from './commands/enable'
import disableCommand from './commands/disable'
import uninstallCommand from './commands/uninstall'

const OPTION_DOMAIN = '--domain'
const OPTION_DOMAIN_DESCRIPTION = `Main domain name to serve site at. Defaults to hostname in production, localhost during development.`

const OPTION_ALIASES = '--aliases'
const OPTION_ALIASES_DESCRIPTION = `Additional domains to respond to (each gets a TLS cert and 302 redirects to main domain). e.g., www.<hostname>`

export default class CommandLineInterface {
  constructor () {
    header(packageInfo.version)
    
    this.commands = sade('kitten')
    
    // Monkey-patch sade’s help so it explicitly exits like the other
    // commands do (so it doesn’t get stuck with the process manager).
    function help (str) {
      this.__help(str)
      process.exit()
    }
    this.commands.__help = this.commands.help
    this.commands.help = help.bind(this.commands)

    // Overwrite the built-in _version() method called by the Sade version
    // command to format the output.
    this.commands._version = function () { 
      // Just exit, the version is printed with the app name on every run.
      process.exit()
    }

    this.commands
      .version(packageInfo.version)
      .describe('A web development kit that’s small, purrs, and loves you.')
  
    this.commands
      .command('serve [pathToServe]', '', {default: true})
      .option(OPTION_DOMAIN, OPTION_DOMAIN_DESCRIPTION)
      .option(OPTION_ALIASES, OPTION_ALIASES_DESCRIPTION)
      .option('--working-directory', 'The working directory Kitten was launched from. Defaults to current directory', '.')
      .describe('Start server as regular process.')
      .action(serveCommand)

    // Service commands.

    this.commands
      .command('deploy <gitHttpsCloneUrl>')
      .option(OPTION_DOMAIN, OPTION_DOMAIN_DESCRIPTION)
      .option(OPTION_ALIASES, OPTION_ALIASES_DESCRIPTION)
      .describe('Deploy Kitten project from Git repository as systemd service.')
      .action(deployCommand)

    this.commands
      .command('status')
      .describe('Show Kitten service status.')
      .action(statusCommand)
    
    this.commands
      .command('logs')
      .describe('Follow Kitten service logs.')
      .option('--since', "Time span to show logs for (e.g., '6 hours ago', 'yesterday')", '1 hour ago')
      .action(logsCommand)
    
    this.commands
      .command('start')
      .describe('Start Kitten systemd service.')    
      .action(startCommand)

    this.commands
      .command('stop')
      .describe('Stop Kitten systemd service.')    
      .action(stopCommand)

    this.commands
      .command('enable')
      .describe('Enable Kitten systemd service (auto-start at boot, etc.)')
      .action(enableCommand)

    this.commands
      .command('disable')
      .describe('Disable Kitten systemd service (auto-start at boot, etc.)')
      .action(disableCommand)

    // Lifecylce commands.

    this.commands
      .command('uninstall')
      .describe('Remove Kitten and all data (including databases from your projects).')
      .action(uninstallCommand)

    // Aliases for global options (to allow people to trigger them without using the double-dashes,
    // for the sake of consistency.)
    this.commands
      .command('version')
      .describe('Alias for --version, -v.')
      .action(_ => this.commands._version())
    
    this.commands
      .command('help [subCommand]')
      .describe('Alias for --help, -h.')
      .action((subCommand = '') => this.commands.help(subCommand))
  }

  parse (args) {
    this.commands.parse(args)  
  }
}

