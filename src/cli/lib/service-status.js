//////////////////////////////////////////////////////////////////////
//
// Function: serviceStatus (synchronous)
//
// Returns the Kitten server daemon status.
//
// Proxies: systemctl status --user kitten
//
//////////////////////////////////////////////////////////////////////

import os from 'os'
import fs from 'fs'
import path from 'path'
import childProcess from 'child_process'
import paths from '../../lib/paths'

export default function serviceStatus () {
  // Note: do not call ensure.systemctl() here as it will
  // ===== create a cyclic dependency. Instead, check for
  //       systemctl support manually before calling status().

  const options = {env: process.env, stdio: 'pipe'}
  
  let isActive
  try {
    childProcess.execSync('systemctl --user is-active kitten', options)
    isActive = true
  } catch (error) {
    isActive = false
  }

  let isEnabled
  try {
    childProcess.execSync('systemctl --user is-enabled kitten', options)
    isEnabled = true
  } catch (error) {
    isEnabled = false
  }

  let daemonDetails = null
  if (isEnabled) {
    // Parse the systemd unit configuration file to retrieve daemon details.
    const configuration = fs.readFileSync(path.join(paths.SYSTEMD_USER_DIRECTORY, 'kitten.service'), 'utf-8').trim().split('\n')

    // const account = configuration[8].trim().replace('User=', '')
    // const execStart = configuration[14].trim().replace('=node ', '=')

    // // Launch configuration.
    // const binaryAndPathBeingServed = /ExecStart=(.*?) (.*?) @hostname/.exec(execStart)
    // const siteJSBinary = binaryAndPathBeingServed[1]
    // const pathBeingServed = binaryAndPathBeingServed[2]

    // // Optional options.
    // let _domain, _aliases
    // const domain = (_domain = /--domain=(.*?)(\s|--|$)/.exec(execStart)) === null ? null : _domain[1]
    // const aliases = (_aliases = /--aliases=(.*?)(\s|--|$)/.exec(execStart)) === null ? null : _aliases[1].split(',')
    // const skipDomainReachabilityCheck = execStart.includes('--skip-domain-reachability-check')
    // const accessLogErrorsOnly = execStart.includes('--access-log-errors-only')
    // const accessLogDisable = execStart.includes('--access-log-disable')

    const optionalOptions = {
      // domain,
      // aliases,
      // skipDomainReachabilityCheck,
      // accessLogErrorsOnly,
      // accessLogDisable
    }

    daemonDetails = {
      // account,
      // siteJSBinary,
      // pathBeingServed,
      // optionalOptions,
      // owncast
    }
  }

  return { isActive, isEnabled, daemonDetails }
}
