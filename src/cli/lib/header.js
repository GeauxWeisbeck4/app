////////////////////////////////////////////////////////////////////////////////
//
// 😸️ Kitten Command Line Interface (CLI) header.
//
// Copyright © 2022-present Aral Balkan, Small Technology Foundation.
// Licensed under GNU AGPL version 3.0.
//
// https://kitten.small-web.org
//
////////////////////////////////////////////////////////////////////////////////

import chalk from 'chalk'

export default function header (version) {
  console.info(chalk.redBright.bold("😸 Kitten"), `v${version}`)
  console.info(chalk.blue.italic('   by Aral Balkan, Small Technology Foundation'))
  console.info('')
  console.info(' ╭───────────────────────────────────────────╮')
  console.info(` │ ${chalk.bold('Like this? Fund us!')}                       │`)
  console.info(' │                                           │')
  console.info(' │ We’re a tiny, independent not-for-profit. │')
  console.info(' │ https://small-tech.org/fund-us            │')
  console.info(' ╰───────────────────────────────────────────╯')
  console.info('')
  console.info(chalk.bold.green('   Need help?'), chalk.green('https://codeberg.org/kitten/app/issues'))
  
  // Sade adds a space above its automatically-generated help output so
  // let’s suppress our own to ensure consistent spacing. Ditto for the
  // version command.
  if (
    !(
      process.argv.includes('help') || 
      process.argv.includes('--help') ||
      process.argv.includes('version') ||
      process.argv.includes('--version')
    )
  ) {
    console.info('')
  }
}
