////////////////////////////////////////////////////////////
//
// Kitten critical assertions.
//
// Copyright ⓒ 2022-present, Aral Balkan (https://ar.al)
// Small Technology Foundation (https://small-tech.org).
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import childProcess from 'child_process'
import chalk from 'chalk'

import tcpPortUsed from 'tcp-port-used'
import serviceStatus from './service-status'

const functions = {
  commandExists, systemctl, journalctl, serviceNotActive, portsAreFree, canEnableService
}
export default functions

function exitWithError (message) {
  console.error(`\n❌ ${chalk.redBright('Error:')} ${message}\n`)
  process.exit(1)
}

// Does the passed command exist? Returns: bool.
export function commandExists (command) {
  try {
    childProcess.execFileSync('which', [command], {env: process.env})
    return true
  } catch (error) {
    return false
  }
}

// Ensure systemctl exists.
export function systemctl () {
  if (!commandExists('systemctl')) {
    exitWithError('systemd not found (systemctl is missing).')
  }
}


// Ensure journalctl exists.
export function journalctl () {
  if (!commandExists('journalctl')) {
    exitWithError('systemd not found (journalctl is missing).')
  }
}

export async function portsAreFree () {
  if (await tcpPortUsed.check(80)) {
    exitWithError('required port (80) is in use.')
  } 
  
  if (await tcpPortUsed.check(443)) {
    exitWithError('required port (443) is in use.')
  }
}

export async function canEnableService () {
  // Ensure systemd exists.
  systemctl()
  journalctl()
  
  // And that the Kitten service is not already active.
  serviceNotActive()
  
  // And that no other service is running on the ports we need (80 and 443).
  await portsAreFree()
}

// Ensures that the server daemon is not currently active.
export function serviceNotActive () {
  // Ensure systemctl exists as it is required for getStatus().
  // We cannot check in the function itself as it would create
  // a circular dependency.
  systemctl()
  const { isActive } = serviceStatus()

  if (isActive) {
    exitWithError(`Kitten Daemon is already running.\n\n${chalk.yellowBright('Please stop it before retrying using:')} kitten ${chalk.greenBright('disable')}\n`)
  }
}
