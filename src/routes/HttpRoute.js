import LazilyLoadedRoute from './LazilyLoadedRoute.js'

export default class HttpRoute extends LazilyLoadedRoute {
  async initialise () {
    // One-time lazy initialisation. Imports the handler and,
    // if present, the page template.
    this._handler = (await import(this.filePath)).default
  }

  async lazilyLoadedHandler (request, response) {
    if (!this._handler) {
      await this.initialise()
    }

    let result = await this._handler(request, response)

    // If a result is returned from an HttpRoute,
    // we assume it should be serialised into JSON
    // (because it’s 2022). If you want to redirect to
    // a GET route, you should write the header in
    // your header. (TODO: Should we provide first-class
    // support for this?)
    result = result ? JSON.stringify(result) : ''

    response.end(result)
  }
}
