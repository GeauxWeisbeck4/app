import { routePatternFromFilePath } from '../Utils'
import staticFile from 'connect-static-file'

export default class StaticRoute {
  /**
   * Represents static routes.
   * 
   * @param {function|string} filePath 
   */
  constructor (filePath) {
    this.filePath = filePath
    this.pattern = routePatternFromFilePath(this.filePath)
    this.method = 'get' 
  }

  // This is a simple static file; serve it as such.
  get handler () {
    return staticFile(this.filePath)
  }
}
