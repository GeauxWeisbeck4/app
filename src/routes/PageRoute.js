import path from 'path'
import LazilyLoadedRoute from './LazilyLoadedRoute.js'
import { developmentTimeHotReloadScript } from '../lib/fragments'

export default class PageRoute extends LazilyLoadedRoute {
  async initialise () {
    // One-time lazy initialisation. Imports the handler and,
    // if present, the page template.
    this._handler = (await import(this.filePath)).default
    this._template = this.defaultTemplate
  }

  async lazilyLoadedHandler (request, response) {
    console.verbose(`About to show page at file path ${this.filePath}`)
    console.verbose(`Base path: ${process.env.basePath}`)

    if (!this._handler) {
      await this.initialise()
    }

    const result = await this._handler(request, response)
    const finalHtml = this.renderHtml(result)
    response.end(finalHtml)
  }

  renderHtml (contents) {
    let styles = null
    let script = null
    let fragments = null
    let libraries = []
    let page = null

    if (contents == undefined) {
      return ''
    }

    if (typeof contents === 'string') {
      // This must be a very simple HTML snippet as a plain
      // string has been returned. Just set it directly.
      page = contents
    } else {
      // This is a more complicated contents. See if only html fragments
      // have been returned or if there is CSS also.
      if (contents instanceof Array) {
        fragments = contents
      } else {
        // OK, so we expect an object otherwise.
        fragments = contents.markup

        // The markup could still be a simple string. Turn it into
        // an array so our expectations later on are met.
        if (!(fragments instanceof Array)) {
          fragments = [fragments]
        }

        // Note that styles could either be a CSS string or, in case of an
        // imported external .styles file, a function that returns a CSS string.
        styles = contents.styles instanceof Function ? contents.styles() : contents.styles

        // We expect the script to be a function instance and add its source to the page.
        script = contents.script != undefined ? `<script>(${contents.script.toString()})()</script>` : null

        // Finally, there might be an array of libraries that the page uses.
        libraries = contents.uses || []
      }

      page = fragments.flat(Infinity).join('\n            ')

      // If there is a script, tack that on too.
      if (script != undefined) {
        page += `\n${script}`
      }

      // If we are in development mode, add support for hot reloads by adding a simple
      // script that connects to the development web socket, detects disconnects,
      // and refreshes the page on reconnect.
      if (!process.env.PRODUCTION) {
        page += developmentTimeHotReloadScript
      }

      // If there are styles, tack them on also, at the end.
      if (styles != undefined) {
        page += `\n${styles}`
      }
    }

    // Render the page inside of the HTML template.

    // TODO: add custom title support.

    // If a custom title is not provided, try to set the title
    // from the h1 tag contents (sans any HTML tags), if it exists. 
    // If not, set it to the path from the project folder.
    //
    // (We try to have beautiful defaults whenever possible.)

    let defaultTitle
    const h1Text = page.match(/\<h1\>(.*?)\<\/h1\>/)
    if (h1Text !== null) {
      defaultTitle = h1Text[1].replace(/\<.*?\>/g, '')
    } else {
      // Attempt to create a passable title by combining the same of the project folder
      // with the path of the page itself.
      const replaceDashesAndUnderscroresWithSpaces = str => str.replace(/-/g, ' ').replace(/_/g, ' ')
      const titleCase = str => str.replace(/\b[a-z]/g, c => c.toUpperCase())
      const removeIndexAndExtension = str => str.replace('index.page', '').replace('.page', '')
      const trimSlashesAndReplaceRemainingOnesWithColons = str => str.replace(/^\//, '').replace(/\/\s*$/, '').replace(/\//g, ': ')

      const prettify = str => titleCase(replaceDashesAndUnderscroresWithSpaces(removeIndexAndExtension(str)))
      const projectTitle = prettify(process.env.basePath.substr(process.env.basePath.lastIndexOf(path.sep) + 1))
      const pageTitle = trimSlashesAndReplaceRemainingOnesWithColons(prettify(this.filePath.replace(process.env.basePath, '')))

      // Only display project name – page title separator if 
      // page title exists (e.g., if it’s index.page, then the title will be empty.)
      const separator = pageTitle.length > 0 ? ' – ' : ' '

      defaultTitle = `${projectTitle}${separator}${pageTitle}`
    }
    const renderedHtml = this._template(page, defaultTitle, libraries)

    const finalHtml = "<!doctype html>\n" + typeof renderedHtml.join === 'function' ? renderedHtml.join('') : renderedHtml

    return finalHtml
  }

  defaultTemplate (page, title, libraries = []) {
    return html`
      <html lang='en'>
      <head>
        <meta charset='UTF-8' />
        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <link rel="icon" href="data:," />
        <title>${raw(title)}</title>
        <!-- Libraries -->
        ${libraries.map(library => (
          html`<script src='/.well-known/library/${library}'></script>`
        ))}
        ${libraries.includes(HTMX) ? html`
          <script>
            htmx.config.useTemplateFragments = true;
          </script>
        ` : ''}
        <!-- /Libraries -->
     </head>
      <body>
        <div id='application'>
          ${raw(page)}
        </div>
      </body>
      </html>
    `
  }
}
