////////////////////////////////////////////////////////////
//
// Kitten Routes
//
// Copyright ⓒ 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

// Conditional logging.
console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}

import path from 'node:path'
import { _findPath } from 'node:module'
import Files from './Files.js'
import { routePatternFromFilePath } from './Utils.js'

import systemExitCodes from './lib/system-exit-codes.js'

import HttpRoute from './routes/HttpRoute.js'
import PageRoute from './routes/PageRoute.js'
import SocketRoute from './routes/WebSocketRoute.js'
import StaticRoute from './routes/StaticRoute.js'

export default class Routes extends EventTarget {
  initialised = false
  routes
  dependencyMap
  broadcastChannel

  constructor () {
    super()
    this.routes = {}
  }

  async initialise () {
    // Get the files in the site being served.
    this.files = new Files()
    this.filesByExtensionCategoryType = await this.files.initialise()

    // Start listening for file events after initialisation for
    // performance reasons (since we ignore events prior to initialisation anyway).
    this.files.addEventListener('file', this.handleFileChange)

    // Create the routes from the files structure.
    this.createRoutes()

    // Flag that we’re initialised (this affects event broadcasts).
    this.initialised = true
    
    return this.routes
  }

  async handleFileChange(event) {
    const { itemType, eventType, itemPath } = event.detail
    if (this.initialised) {
      // When anything changes in the project, we simply exit. Restarting
      // the Kitten service is handled by the Kitten Process Manager during
      // development and by systemd in production.
      console.info(`🔔 ${itemType.charAt(0).toUpperCase()+itemType.slice(1)} ${eventType} (${itemPath.replace(process.env.basePath, '')}), exiting to trigger reload.`)
      process.exit(systemExitCodes.restartRequest)
    } else {
      // Note: this should never be reached. Remove?
      console.verbose('[handleFileChange]', itemPath, 'ignoring', 'not initialised')
    }
  }
  
  createRoutesOfCategoryType (categoryType) {
    const category = this.filesByExtensionCategoryType[`${categoryType}Routes`]
    const extensions = Object.keys(category)

    for (const extension of extensions) {
      const filePaths = category[extension]
      for (const filePath of filePaths) {
        const extension = path.extname(filePath).replace('.', '')

        const RouteType = categoryType === 'static'
          ? StaticRoute
          : {
              'page': PageRoute,
              'socket': SocketRoute
            }[extension] || HttpRoute

        if (this.routes[extension] === undefined) {
          this.routes[extension] = []
        }
        const pattern = routePatternFromFilePath(filePath)
        this.routes[extension][pattern] = new RouteType(filePath)
      }
    }
  }
  
  // Create the routes and add them to the server.
  // The ESM Loaders will automatically handle any processing that needs to
  // happen during the import process.
  createRoutes () {
    // Add static routes.
    this.createRoutesOfCategoryType('static')
    
    // Add dynamic routes.
    this.createRoutesOfCategoryType('dynamic')

    // Log the routes if in verbose mode.
    console.verbose('Routes', this.routes, '\n')
  }
}
