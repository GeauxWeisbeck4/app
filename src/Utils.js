import os from 'os'
import fs from 'fs'
import path from 'path'
import childProcess from 'child_process'
import process from 'process'

console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}

// Calculate the base path used by the Kitten server to find files in the served app.
export function setBasePath (workingDirectory, pathToServe) {
  // Resolve the path to serve so that it works both when run as
  // kitten <path to serve> from anywhere and, from the source folder, as
  // bin/kitten <path to serve>.
  let basePath = path.resolve(workingDirectory, pathToServe)

  if (!fs.existsSync(basePath)) {
    throw new Error(`${basePath} – path does not exist.`)
  }

  // You can place your source files either in the project
  // folder directly or in a subfolder called src. Use the 
  // latter if it exists.
  const srcFolder = path.join(basePath, 'src')
  basePath = fs.existsSync(srcFolder) ? srcFolder : basePath

  // Set the basePath as an environment variable so the ESM Module Loader
  // can access it also.
  process.env.basePath = basePath

  return basePath
}

//
// This function can be called from either the main process or the Node ES Loader,
// and relies on process.env.basePath being set by the main process prior to use.
//

export const HTTP_METHODS = ['get', 'head', 'patch', 'options', 'connect', 'delete', 'trace', 'post', 'put']
export const BACKEND_EXTENSIONS = HTTP_METHODS.concat(['socket'])
export const FRONTEND_EXTENSIONS = ['page']
export const DEPENDENCY_EXTENSIONS = ['component', 'fragment', 'script', 'styles']
export const DYNAMIC_ROUTE_EXTENSIONS = BACKEND_EXTENSIONS.concat(FRONTEND_EXTENSIONS)
export const STATIC_ROUTE_EXTENSIONS = ['html', 'htm']

// TODO: This is getting messy. The whole route categorisation system needs
// a rethink following the change to implement seamless handling of static routes.
export const ALL_ROUTE_EXTENSIONS = DYNAMIC_ROUTE_EXTENSIONS

export const extensionCategories = {
  backendRoutes: BACKEND_EXTENSIONS,
  frontendRoutes: FRONTEND_EXTENSIONS,
  dependencies: DEPENDENCY_EXTENSIONS,
  dynamicRoutes: DYNAMIC_ROUTE_EXTENSIONS,
  staticRoutes: STATIC_ROUTE_EXTENSIONS,
  allRoutes: ALL_ROUTE_EXTENSIONS
}

export const supportedExtensionsRegExp = `\.(${DYNAMIC_ROUTE_EXTENSIONS.concat(STATIC_ROUTE_EXTENSIONS).join('|')})$`

const indexWithExtensionRegExp = new RegExp(`index${supportedExtensionsRegExp}`)
const indexWithPropertiesRegExp = /\/index_(?!\/)/
const extensionRegExp = new RegExp(supportedExtensionsRegExp)

export function extensionOfFilePath (filePath) {
  return path.extname(filePath).replace('.', '')
}

export function classNameFromFilePath (filePath) {
  return classNameFromRoutePattern(routePatternFromFilePath(filePath))
}

export function routePatternFromFilePath (filePath) {
  const basePath = process.env.basePath

  return filePath
    .replace(basePath, '')                   // Remove the base path.
    .replace(indexWithPropertiesRegExp, '/') // Handle index files that contain properties.
    .replace(/_/g, '/')                      // Replace underscores with slashes.
    .replace(/\[(.*?)\]/g, ':$1')            // Replace properties. e.g., [prop] becomes :prop.
    .replace(indexWithExtensionRegExp, '')   // Remove index path fragments (and their extensions).
    .replace(extensionRegExp, '')            // Remove extension.
}

// Converts a route in the form of, e.g.,
// '/some_thing/with/underscores-and-hyphens' to
// SomeThingWithUnderscoresAndHyphensPage
export function classNameFromRoutePattern (pattern) {
  const className = pattern
    .split('/').join('*')
    .split('-').join('*')
    .split('_').join('*')
    .split(':').join('*')
    .split('*')
    .map(fragment => fragment.charAt(0).toUpperCase() + fragment.slice(1))
    .join('')
    + 'Page'

  return className === 'Page' ? 'IndexPage' : className
}

// Format the Kitten app path in a way that works regardless of 
// whether Kitten was invoked using the global command
// from the bundled distribution or via bin/kitten from source.
export const kittenAppPath = process.argv[1]
  .replace('kitten-bundle.js', '')          // When running in production.
  .replace('kitten-process-manager.js', '') // When running in development.
  .replace('src/processes/main.js', '')     // When running from source.
  .replace(/tests\/.*?$/, '')               // Ditto (when running from tests).
