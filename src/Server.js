////////////////////////////////////////////////////////////
//
// Kitten Server.
//
// Copyright © 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

// Conditional logging.
console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}

import fs from 'node:fs'
import fsAsync from 'node:fs/promises'
import path from 'node:path'
import process from 'node:process'
import childProcess from 'node:child_process'
import { _findPath } from 'node:module'

import chalk from 'chalk'
import Prism from 'prismjs'

import polka from 'polka'
import bodyParser from 'body-parser'
import staticFile from 'connect-static-file'

import { kittenAppPath } from './Utils.js'
import { developmentTimeHotReloadScript } from './lib/fragments.js'

import https from '@small-tech/https'
import { tinyws } from 'tinyws'

import paths from './lib/paths.js'
import Globals from './lib/globals.js'
import Routes from './Routes.js'

export default class Server extends EventTarget {
  constructor (options = {}) {
    super()

    // Determine which places to listen at.
    let domains = []
    
    // If a set of domains to listen on is provided use that (this means
    // we’re serving in production). Otherwise, default to listening on
    // local interfaces.
    const domain = options.domain || 'localhost'
    domains.push(domain)

    if (domain !== 'localhost' && options.aliases) {
      // We’re running in production and a set of aliases has been passed,
      // also include those in the set of domains to listen for.
      domains = domains.concat(options.aliases.split(','))
    }

    // You can pass a shorthand alias, www, to stand for www.<domain>.
    // Rewrite that as the full domain URL if we encounter it.
    domains = domains.map(value => value === 'www' ? `www.${domain}` : value)

    this.options = { domains }

    // Create the app.

    // TODO: Move this out of here. Ideally into a Kitten page.
    const errorStyles = `
      <style>
        :root {
          --light-violet: rgb(253, 240, 253);
        }
        body {
          display: grid;
          grid-template-areas:
            "kitten            error-message"
            "stack-trace-title stack-trace-title"
            "stack-trace       stack-trace";
          grid-template-rows: auto 1fr;
          /*
            The minmax() is there to stop the pre blocks from blowing
            out of the grid in Firefox (not an issue without on WebKit/Chromium).
          */
          grid-template-columns: minmax(0, auto) minmax(0, 1fr);
          max-width: 920px;
          margin-left: auto;
          margin-right: auto;
          padding: 2em;
          font-family: sans-serif;
          font-size: 1.5em;
          padding: 1em;
        }
        h1 {
          color: deeppink;
          font-size: clamp(3rem, 0.48rem + 9.6vw, 6rem);
          margin: 0;
        }
        h2 {
          color: darkviolet;
          margin-top: 0;
          font-size: clamp(1rem, 0.16rem + 3.2vw, 2rem);
        }
        h3 {
          grid-area: stack-trace-title;
          font-size: clamp(1.125rem, 0.18rem + 3.6vw, 2.25rem);
          background-color: darkviolet;
          color: white;
          margin: 0;
          border-radius: 0.66em 0.66em 0 0;
          padding: 0.75em 0.5em 0.5em 0.5em;
        }
        h4 {
          font-size: clamp(0.75rem, 0.12rem + 2.4vw, 1.5rem);
          background-color: deeppink;
          color: white;
          margin: -1.5rem -1.5rem 1.5rem -1.5rem;
          padding: 0.25em 0.66em;
        }
        .solitary-stack-frame-separator h4 {
          margin-top: -1em !important;
        }
        #kitten-stack-frames h4 {
          margin-top: 1.5em;
        }
        a {
          text-decoration: none;
          color: black;
          border-bottom: 2px solid deepskyblue;
        }
        img {
          grid-area: kitten;
          height: clamp(7.5rem, -0.9rem + 32vw, 17.5rem);
        }
        ul {
          list-style-type: none;
          padding-left: 1em;
        }
        li {
          font-size: clamp(1rem, 0.895rem + 0.4vw, 1.125rem);
          margin-bottom: 1em;
        }
        li:not(:last-of-type) {
          padding-bottom: 1em;
        }
        pre[class*="language-"] {
          margin: 2em 0;
        }
        code {
          font-family: monospace;
          font-size: 1em;
          font-weight: 600;
        }
        #error-message {
          grid-area: error-message;
          margin-left: 1em;
        }
        #stack-trace {
          grid-area: stack-trace;
          border: 2px solid darkviolet;
          border-radius: 0 0 1em 1em;
          margin: 0;
          padding: 1em;
        }
        .stack-location {
          display: block;
          font-size: 0.9em;
        }
        .error-line {
          font-weight: 900;
        }
        span.error-column::before {
          color: deeppink;
          content: '^';
          position: absolute;
          bottom: 33%;
        }
        </style>
    `

    const generalErrorTemplate = `
      <html lang="en">
      <head>
        <meta charset='utf-8' />
        <meta name='viewport' content='width=device-width' />
        <title>Error {CODE}</title>
        <link rel='stylesheet' type='text/css' href='/.well-known/library/prism.css' />
        ${errorStyles}
      </head>
      <body>
        <img src='/.well-known/kitten/images/kitten-playing.svg' />
        <div id='error-message'>
          <h1>{CODE}</h1>
          <h2>{ERROR}</h2>
        </div>
        <h3>Stack trace</h3>
        <ul id='stack-trace'>
          {STACK}
        </ul>
        ${developmentTimeHotReloadScript}
        </body>
      </html>
    `

    const pageNotFoundErrorPageStyles = `
      <style>
        body {
          max-width: clamp(20rem, 3.2rem + 64vw, 40rem);
          margin-left: auto;
          margin-right: auto;
          padding: 2em;
          font-family: sans-serif;
          font-size: 1.5em;
          padding: 1em;
          text-align: center;
        }
        h1 {
          color: deeppink;
          font-size: clamp(3rem, 0.48rem + 9.6vw, 6rem);
          margin: 0;
        }
        h2 {
          color: darkviolet;
          margin-top: 0;
          font-size: clamp(1rem, 0.16rem + 3.2vw, 2rem);
        }
        a {
          text-decoration: none;
          color: black;
          border-bottom: 2px solid deepskyblue;
        }
        img {
          grid-area: kitten;
          height: clamp(20rem, 3.2rem + 64vw, 40rem);
        }
      </style>
    `

    const pageNotFoundErrorPage = `
      <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Error 404</title>
        ${pageNotFoundErrorPageStyles}
      </head>
      <body>
        <img src='/.well-known/kitten/images/kitten-sitting.svg' />
        <h1>404</h1>
        <h2>Page not found.</h2>
        ${developmentTimeHotReloadScript}
        </body>
      </html>
    `

    this.app = polka({
      onError: async (error, _request, response, _next) => {
        if (error.status === 404) {
          response.end(pageNotFoundErrorPage)
          return
        }

        // TODO: Only show detailed error information (stack trace, etc.)
        // in development mode.

        // If we’re here, the error occured while page was initially loading
        // so display the basic error template.
        const errorCode = error.code || error.status || 500
        const errorNumericCode = parseInt(errorCode)
        response.statusCode = Number.isNaN(errorNumericCode) ? 500 : errorNumericCode
        
        // Let’s make the stack trace nicer.
        let stackFramesSeparated = false
        let lastIndexOfAppStackFrames = 0

        const kittenStackFrameRegExp = /\(.*small-tech\.org\/kitten\/app/
        const nodeModuleRegExp = /\/node_modules\/(.*?)\//

        console.error(chalk.bold.red(`🞫 Error ${errorCode}: ${error.message}`))

        const stackLines = error.stack.split("\n")
        
        // At the top of the stack trace there _might_ be the actual
        // line of code that caused the error and a line with a caret
        // symbol pointing to it. This will then be followed by the error message.
        //
        // Note: the “might” part might be a bug in EcmaScript Module Loaders
        // in Node.js. TODO: Reproduce with a minimal example and file a bug.
        let stackHeaderIndex = 0
        while (!stackLines[stackHeaderIndex].includes(error.message)) {
          stackHeaderIndex++
        }

        const stackHeaderLines = stackLines.splice(0, stackHeaderIndex+1)

        for (let lineIndex = 0; lineIndex < stackLines.length; lineIndex++) {
          const stackLine = stackLines[lineIndex]
          const isKittenStackFrame = stackLine.match(kittenStackFrameRegExp) !== null
          const isNodeModule = stackLine.match(nodeModuleRegExp) !== null

          // Display source for Kitten stack frames that aren’t related
          // to Node modules (we simply link those to npm, to help in case
          // the person wants to quickly find more information about them).
          let snippetOfConcern = null
          let snippetStartLine = 0
          let snippetEndLine = 0
          if (!isKittenStackFrame && !isNodeModule) {
            const sourceDetails = stackLine.match(/file:\/\/(.*?):(\d*?):(\d*?)\)/)
            if (sourceDetails !== null) {
              const sourceFilePath = sourceDetails[1]
              const errorLine = parseInt(sourceDetails[2])
              const errorColumn = parseInt(sourceDetails[3])
              const source = await fsAsync.readFile(sourceFilePath, {encoding: 'utf-8'})
              const sourceLines = source.split("\n")
            
              const snippetNumberOfLookaroundLines = 3
              snippetStartLine = errorLine - snippetNumberOfLookaroundLines - 1
              snippetEndLine = errorLine + snippetNumberOfLookaroundLines

              // Make sure we don’t overflow the bounds of the source file.
              const firstLineIndexOfSourceFile = 0
              const lastLineIndexOfSourceFile = sourceLines.length - 1

              snippetStartLine = snippetStartLine < firstLineIndexOfSourceFile ? 0 : snippetStartLine
              snippetEndLine = snippetEndLine > lastLineIndexOfSourceFile ? lastLineIndexOfSourceFile : snippetEndLine

              const snippetOfConcernLines = sourceLines.slice(snippetStartLine, snippetEndLine)

              // Mark up the code prior to syntax highlighting it to demarcate the error line
              // and error column. Do this using JavaScript multiline comment syntax so we can
              // strip these tokens away and replace them after the syntax highlighting.

              // TODO: Handle edge case of top/bottom of file.
              const lineIndexToHighlight = Math.floor(snippetOfConcernLines.length/2)
              let lineToHighlight = snippetOfConcernLines[lineIndexToHighlight]
            
              // Mark up the column location.
              const lineToHighlightLetters = lineToHighlight.split('')
              lineToHighlightLetters[errorColumn-1] = `/*ERROR_COLUMN_START*/${lineToHighlightLetters[errorColumn-1]}`
              // The ERROR_COLUMN_END marker is at the end of the line so as not to
              // mess up the code and thus the tokeniser/highlighting. It doesn’t matter where it
              // goes as long as it goes after the ERROR_COLUMN_START marker as the start position
              // is the only one we’re actually interested in.
              lineToHighlight = `/*ERROR_LINE_START*/${lineToHighlightLetters.join('')}/*ERROR_COLUMN_END*/ /*ERROR_LINE_END*/`
              snippetOfConcernLines[lineIndexToHighlight] = lineToHighlight

              snippetOfConcern = snippetOfConcernLines.join("\n")
            
              // We can’t use the Prism line numbers plugin in Node directly as it uses DOM and is meant for the browser.
              // Instead, here’s a workaround based on https://stackoverflow.com/a/59577306i
            
              // https://github.com/PrismJS/prism/blob/master/plugins/line-numbers/prism-line-numbers.js#L109
              let NEW_LINE_EXP = /\n(?!$)/g
              let lineNumbersWrapper

              Prism.hooks.add('after-tokenize', function (env) {
                var match = env.code.match(NEW_LINE_EXP)
                var linesNum = match ? match.length + 1 : 1
                var lines = new Array(linesNum + 1).join('<span></span>')

                lineNumbersWrapper = `<span aria-hidden="true" class="line-numbers-rows">${lines}</span>`
              })

              snippetOfConcern = Prism.highlight(snippetOfConcern, Prism.languages.javascript, 'javascript')
            
              const ERROR_LINE_START_TOKEN_AFTER_PRISM = '<span class="token comment">/*ERROR_LINE_START*/</span>'
              const ERROR_LINE_END_TOKEN_AFTER_PRISM = '<span class="token comment">/*ERROR_LINE_END*/</span>'
              const ERROR_COLUMN_START_TOKEN_AFTER_PRISM = '<span class="token comment">/*ERROR_COLUMN_START*/</span>'
              const ERROR_COLUMN_END_TOKEN_AFTER_PRISM = '<span class="token comment">/*ERROR_COLUMN_END*/</span>'

              snippetOfConcern = snippetOfConcern.replace(ERROR_LINE_START_TOKEN_AFTER_PRISM, "<span class='error-line'>")
              snippetOfConcern = snippetOfConcern.replace(ERROR_LINE_END_TOKEN_AFTER_PRISM, "</span>")
              snippetOfConcern = snippetOfConcern.replace(ERROR_COLUMN_START_TOKEN_AFTER_PRISM, "<span class='error-column'>")
              snippetOfConcern = snippetOfConcern.replace(ERROR_COLUMN_END_TOKEN_AFTER_PRISM, "</span>")

              snippetOfConcern = snippetOfConcern + lineNumbersWrapper
            }
          }

          if (!stackFramesSeparated && isKittenStackFrame) {
            lastIndexOfAppStackFrames = lineIndex
            stackFramesSeparated = true
          }
          stackLines[lineIndex] = stackLine
            .replace(/</g, '&lt;') // Encode any angular brackets
            .replace(/>/g, '&gt;') // there may be prior to marking it up.
            .replace(/^\s*at\s*/, '<li>While running ')
            .replace(/$/, '</li>')
            .replace(`file://${process.env.basePath}`, '')
            .replace(/\<li\>(.*?)\(/, '<li><code>$1</code>(')
            .replace(/<\/code>(.*?)<\/li>/, '</code><span class=\'stack-location\'>$1</span></li>')
            .replace(kittenStackFrameRegExp, 'in ')
            .replace(/\/node_modules\/(.*?)\//, ' module <a target=\'_blank\' href=\'https://www.npmjs.com/package/$1\'>$1</a> /')
            .replace(/\(/, 'in ')
            .replace(/:(\d*?):(\d*?)\)<\/span><\/li>$/, ' <em>(line $1, column $2)</em>')

          if (snippetOfConcern) {
            stackLines[lineIndex] = stackLines[lineIndex].replace('<li>', `<li><pre style='--start-value: ${snippetStartLine}' class='language-javascript line-numbers'><code class='language-javascript'>${snippetOfConcern}</code></pre>`)
          }
        }

        // Add the exact code location if we have it to the top stack frame.
        if (stackHeaderLines.length > 1) {
          // OK, we have a location; let’s format it.
          let codeWithError = stackHeaderLines[1]
          let errorColumnLocation = stackHeaderLines[2]
          const padding = codeWithError.match(/\s+/)[0]
          codeWithError = codeWithError.replace(padding, '')
          errorColumnLocation = errorColumnLocation.replace(padding, '')
          const lineWithReintroducedAt = stackLines[0].replace(/<li><code>/, '<li><code>at ')
          stackLines[0] = ` <pre><code>${codeWithError}</code></pre><pre><code>${errorColumnLocation}</code></pre>${lineWithReintroducedAt}`
        }
        
        // If we have stack frames both from the app/site being served and from
        // Kitten itself, we separate them visually in the output to make it
        // easier for people to debug things and focus on the important bits.
        if (!stackFramesSeparated || (stackFramesSeparated && lastIndexOfAppStackFrames > 0)) {
          const solitarySeparatorMark = stackFramesSeparated ? `class='solitary-stack-frame-separator'` : ''
          stackLines.unshift(`<section ${solitarySeparatorMark} id='app-stack-frames'><h4>From your app</h4>`)
        }
        if (stackFramesSeparated) {
          const solitarySeparatorMark = lastIndexOfAppStackFrames === 0 ? `class='solitary-stack-frame-separator'` : ''
          const insertionPoint = lastIndexOfAppStackFrames === 0 ? 0 : lastIndexOfAppStackFrames + 1
          stackLines.splice(insertionPoint, 0, `</section><section ${solitarySeparatorMark} id='kitten-stack-frames'><h4>From Kitten</h4>`)
          stackLines.push('</section>')
        }

        const prettyStackTraceHtml = stackLines.join("\n")
        
        const errorPage = generalErrorTemplate
          .replace(/\{CODE\}/g, errorCode)
          .replace('{ERROR}', error.toString())
          .replace('{STACK}', prettyStackTraceHtml)
        response.end(errorPage)
      }
    })

    // Add body parsing middleware.
    const urlencodedParser = bodyParser.urlencoded({ extended: false })
    const jsonParser = bodyParser.json()
    this.app.use(urlencodedParser)
    this.app.use(jsonParser)

    // Add the WebSocket server.
    this.app.use(tinyws())
  }

  async initialise () {
    //
    // Add static htmx routes to the server.
    //
    const wellKnownPath = '/.well-known'
    const wellKnownDevelopmentSocketPath = `${wellKnownPath}/development-socket`
    const wellKnownLibraryPath = `${wellKnownPath}/library`
    const wellKnownKittenImagesPath = `${wellKnownPath}/kitten/images`
    const htmxBasePath = path.join(kittenAppPath, 'node_modules', 'htmx.org', 'dist')
    const kittenImagesPath = path.join(kittenAppPath, 'images')

    // If we’re in development mode, create the development WebSocket route that’s
    // used to detect server restarts and refresh pages.
    if (!process.env.PRODUCTION) {
      this.app.use(wellKnownDevelopmentSocketPath, async (request, _response, next) => {
        if (request.ws) {
          await request.ws()
        } else {
          next()
        }
      })
    }

    // Base htmx library.
    const htmxPath = path.join(htmxBasePath, 'htmx.min.js.gz')
    this.app.get(`${wellKnownLibraryPath}/htmx`, staticFile(htmxPath, { encoded: 'gzip' }))

    // htmx web socket extension (TODO: gzip this too or is it already small enough?)
    // const htmxWebSocketExtensionPath = path.join(htmxBasePath, 'ext', 'ws.js')
    // this.app.get(`${wellKnownLibraryPath}/htmx-websocket-extension`, staticFile(htmxWebSocketExtensionPath))

    // Using my development version of the htmx WebSocket extension for now.
    // TODO: Contribute changes upstream once I have things working.
    const htmxWebSocketExtensionMyVersionPath = path.join(kittenAppPath, 'src', 'routes', 'lib', 'ws.js')
    this.app.get(`${wellKnownLibraryPath}/htmx-websocket-extension`, staticFile(htmxWebSocketExtensionMyVersionPath))

    // Using my development version of hyperscript right now, until this is resolved:
    // https://github.com/bigskysoftware/_hyperscript/issues/370
    const hyperscriptMyVersionPath = path.join(kittenAppPath, 'node_modules', 'hyperscript.org', 'dist', '_hyperscript.min.js.gz')
    this.app.get(`${wellKnownLibraryPath}/hyperscript`, staticFile(hyperscriptMyVersionPath, { encoded: 'gzip' }))
    
    // Add the Prism theme css file
    const prismThemeFilePath = path.join(kittenAppPath, 'css', 'prism.css')
    this.app.get(`${wellKnownLibraryPath}/prism.css`, staticFile(prismThemeFilePath))
    
    // TODO: Add support for hyperscript extensions like socket, workers, etc.
    
    // Add Kitten images used in error pages, etc.
    // /.well-known/kitten/images/kitten-playing.svg, etc.
    const kittenImageStaticFileHandler = name => staticFile(path.join(kittenImagesPath, `kitten-${name}.svg`))
    const kittenImageUrlPath = name => path.join(`${wellKnownKittenImagesPath}/kitten-${name}.svg`)

    this.app.get(kittenImageUrlPath('head'), kittenImageStaticFileHandler('head'))
    this.app.get(kittenImageUrlPath('sitting'), kittenImageStaticFileHandler('sitting'))
    this.app.get(kittenImageUrlPath('playing'), kittenImageStaticFileHandler('playing'))
    
    // If the project uses node modules but they have not been installed yet,
    // install them using our own built-in npm.
    const projectPackageFilePath = path.join(process.env.basePath, 'package.json')
    const projectNodeModulesDirectoryPath = path.join(process.env.basePath, 'node_modules')
    if (fs.existsSync(projectPackageFilePath)) {
      // OK, the project has a package file, lets see if it already has a
      // node_modules directory. If so, we assume the dependencies have been
      // installed (we do not support updating of dependencies at server start).
      if (!fs.existsSync(projectNodeModulesDirectoryPath)) {
        // There is no node_modules directory, so let’s see if the project
        // actually has any dependencies that need to be installed. If so, we’ll
        // install them.
        const projectPackage = JSON.parse(fs.readFileSync(projectPackageFilePath, 'utf-8'))
        if (projectPackage.dependencies != undefined) {
          // Install node modules for project being served.
          const kittenNpm = path.join(paths.KITTEN_RUNTIME_DIRECTORY, 'bin', 'npm')
          childProcess.execSync(`${kittenNpm} install`, { 
            encoding: 'utf-8',
            env: { NODE_OPTIONS: '' },
            cwd: process.env.basePath
          })
        }
      }
    }

    // Initialise globals. These will be available for us from all routes.
    Globals.initialise()
    
    // If there is a main.script in the project, import it. Since it could
    // be asynchronous, we await it.
    const mainScriptPath = path.join(process.env.basePath, 'main.script')
    if (fs.existsSync(mainScriptPath)) {
      const mainScript = await import(mainScriptPath)
      await mainScript.default(this.app)
    }

    // Get routes from project being served.
    this.routes = await (new Routes()).initialise()

    // Add routes to server.
    for (const routesOfType of Object.values(this.routes)) {
      for (const [pattern, route] of Object.entries(routesOfType)) {
        console.verbose('Adding route', route.method, pattern, route.handler)
        this.app[route.method](pattern, route.handler)
      }
    }

    // Prepare some nice data we can present to the person running the server.
    const domain = this.options.domains[0]
    
    // If there are still domains left, these are aliases.
    const aliases = this.options.domains.slice(1)
    let aliasesIfAny = ''
    if (aliases.length > 0) {
      // Show a list of the aliases (just the subdomains).
      const numAliases = aliases.length
      const prettyAliases = aliases.map(alias => alias.replace(`.${domain}`, '')).join(', ')
      aliasesIfAny = `(and subdomain${numAliases > 1 ? 's' : ''} ${prettyAliases})`
    }

    // Get the handler from the Polka instance and create a secure site using it.
    // (Certificates are automatically managed by @small-tech/https).
    const { handler } = this.app

    this.server = https.createServer(this.options, handler)
    this.server.listen(443, () => {
      console.info(
        `🌍 ${chalk.bold('Domain  ')} https://${domain} ${aliasesIfAny}\n` +
        `📂 ${chalk.bold('Source  ')} ${process.env.basePath}\n` +
        `💾 ${chalk.bold('Data    ')} ${paths.KITTEN_CURRENT_DATABASE_SYMLINK_PATH}\n` +
        `🔑 ${chalk.bold('Password')} ${db.kitten.password}\n`
      )
    })
  }
}
