// Binary and configuration path usage adheres to 
// freedesktop.org XDG Base Directory Specification
// https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
import path from 'node:path'

const BINARY_HOME = path.join(process.env.HOME, '.local', 'bin')
const KITTEN_BINARY_PATH = path.join(BINARY_HOME, 'kitten')

const DATA_HOME = path.join(process.env.XDG_DATA_HOME || process.env.HOME, '.local', 'share')
const SMALL_TECH_DATA_HOME = path.join(DATA_HOME, 'small-tech.org')

const KITTEN_DATA_HOME = path.join(SMALL_TECH_DATA_HOME, 'kitten')
const KITTEN_APP_DIRECTORY_PATH = path.join(KITTEN_DATA_HOME, 'app')
const KITTEN_DIST_DIRECTORY_PATH = path.join(KITTEN_APP_DIRECTORY_PATH, 'dist')

const KITTEN_TEMP_DIRECTORY = path.join(path.sep, 'tmp', 'small-tech.org', 'kitten')
const KITTEN_TEMP_RUNTIME_ARCHIVE_PATH = path.join(KITTEN_TEMP_DIRECTORY, 'runtime.tar.xz')

const KITTEN_RUNTIME_DIRECTORY = path.join(KITTEN_DATA_HOME, 'runtime')
const KITTEN_DATABASES_DIRECTORY = path.join(KITTEN_DATA_HOME, 'databases')
const KITTEN_CURRENT_DATABASE_SYMLINK_PATH = path.join(KITTEN_DATA_HOME, 'database')
const KITTEN_DEPLOYMENTS_DIRECTORY = path.join(KITTEN_DATA_HOME, 'deployments')

const CONFIG_HOME = path.join(process.env.XDG_CONFIG_HOME || process.env.HOME, '.config')
const SYSTEMD_USER_DIRECTORY = path.join(CONFIG_HOME, 'systemd', 'user')
const KITTEN_SYSTEMD_UNIT_PATH = path.join(SYSTEMD_USER_DIRECTORY, 'kitten.service')

const paths = {
  BINARY_HOME,
  KITTEN_BINARY_PATH,

  DATA_HOME,
  SMALL_TECH_DATA_HOME,

  KITTEN_DATA_HOME,
  KITTEN_APP_DIRECTORY_PATH,
  KITTEN_DIST_DIRECTORY_PATH,

  KITTEN_TEMP_DIRECTORY,
  KITTEN_TEMP_RUNTIME_ARCHIVE_PATH,

  KITTEN_RUNTIME_DIRECTORY,
  KITTEN_DATABASES_DIRECTORY,
  KITTEN_CURRENT_DATABASE_SYMLINK_PATH,
  KITTEN_DEPLOYMENTS_DIRECTORY,

  CONFIG_HOME,
  SYSTEMD_USER_DIRECTORY,
  KITTEN_SYSTEMD_UNIT_PATH
}

export default paths
