////////////////////////////////////////////////////////////////////////////////
//
// Exit status codes for system programs that attempts to conform to the nearest
// thing I could find to a standard which is used in C/C++, via sysexists.h.
// (See: https://tldp.org/LDP/abs/html/exitcodes.html)
//
// Any additions to this will be documented here:
//
//   - 99: Restart request.
//
// Original sysexists.h, where some portions of the comments in this file are
// from, is copyright © 1987, 1993 The Regents of the University of California.
// All rights reserved. And released under the 3-Clause BSD License.
//
// Copyright ⓒ 2022-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////////////////////////

export default {
  ok:   0,
  // This is used for successful exits.

  generalError: 1,
  // This is a catchall for general errors. Note that in the C/C++ ~standard
  // error 69 is also used as a catchall.

  cliSyntaxError: 64,
  // Command-line interface was used incorrectly, e.g., with
  // the wrong number of arguments, a bad flag, bad syntax in a parameter, etc.

  dataError: 65,
  // Input data was incorrect in some way. This should only be
  // used for personal data and not system files.

  fileNotFound: 66,
  // An input file (not a system file) did not
  // exist or was not readable.

  accountNotFound: 67,
  // The specified account count not be found.

  hostNotFound: 68,
  // The specified host could not be found.

  unavailable: 69,
  // A service is unavailable. This can occur if a support program
  // or file does not exist. This can also be used as a catchall message
  // when something you wanted to do doesn’t work, but you don’t know why.

  internalSoftwareError: 70,
  // An internal software error has been detected.
  // This should be limited to non-operating system related errors as possible.

  systemError: 71,
  // An operating system error has been detected.
  // This is intended to be used for such things as "cannot
  // fork", "cannot create pipe", or the like.  It includes
  // things like getuid returning a user that does not
  // exist in the passwd file.

  criticalSystemFileMissing: 72,
  // A critical system file (e.g., /etc/passwd, /etc/utmp, etc.) does not exist,
  // cannot be opened, or has some sort of error (e.g., syntax error).

  cannotCreateOutputFile: 73,
  // A specified output file cannot be created.

  inputOutputError: 74,
  // An error occurred while doing I/O on some file.

  temporaryFailure: 75,
  // Temporary failure, indicating something that the operation
  // should be retried at a later time.

  protocolError: 76,
  // Remote system returned something that was not possible during a protocol exchange.

  permissionDenied: 77,
  // You did not have sufficient permission to perform the operation.
  // This is not intended for file system problems, which should use
  // fileNotFound or cannotCreateOutputFile, but for higher-level permissions.

  configurationError: 78,
  // There was an error in the app’s configuration that means we cannot proceed.
  
  restartRequest: 99,
  // The process is requesting a restart. Used when process knows its being run
  // by a process manager and is asking for a restart.
}
