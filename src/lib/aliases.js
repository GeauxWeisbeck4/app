////////////////////////////////////////////////////////////////////////////////
//
// 😸️ Kitten JavaScript route aliases.
//
// Files with these extensions are supported as server-side JavaScript
// routes by Kitten. Files with any other extensions are served
// as static files.
//
// Copyright © 2022-present Aral Balkan, Small Technology Foundation.
// Licensed under GNU AGPL version 3.0.
//
// https://kitten.small-web.org
//
////////////////////////////////////////////////////////////////////////////////

function truthyHashmapFromArray(array) {
  return array.reduce((obj, key) => { obj[key] = true; return obj}, {})
}

const _aliases = [
  '.page',
  '.component',
  '.fragment',
  '.styles',
  '.script',
  '.get',
  '.head',
  '.patch',
  '.options',
  '.connect',
  '.delete',
  '.trace',
  '.post',
  '.put',
  '.socket'
]

const aliases = truthyHashmapFromArray(_aliases)
export default aliases
