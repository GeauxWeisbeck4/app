export const developmentTimeHotReloadScript = `
    <script>
      // Kitten development time hot reload script.
      let connectionCount = 0
      let __kittenDevelopmentSocket
      let __kittenDevelopmentSocketReconnectionAttemptTimeoutId = null

      function __kittenConnectToDevelopmentSocket () {
        console.log('Attempting to connect to Kitten development socket.')
        __kittenDevelopmentSocket = new WebSocket(\`wss://\${location.host}/.well-known/development-socket\`)

        const socketOpenHandler = _ => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          connectionCount++
          if (connectionCount === 2) {
            // Reconnected after Kitten restart. Ask for page reload.
            window.location.reload()
          } else {
            console.log('😸 Kitten: connected to development-time hot reload socket.')
          }
        }

        const socketCloseHandler = _ => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          __kittenDevelopmentSocketReconnectionAttemptTimeoutId = setTimeout(__kittenConnectToDevelopmentSocket, 10)
        }

        __kittenDevelopmentSocket.addEventListener('open', socketOpenHandler)
        __kittenDevelopmentSocket.addEventListener('close', socketCloseHandler)

        // This is mainly a fix for Firefox, which is the only browser that results in
        // the closing of the WebSocket connection at the start of an HTTP load request
        // (e.g., form POST) and subsequent reconnection, thereby firing of a premature
        // GET request to the server.
        // See: https://mastodon.ar.al/@aral/109104595295584049
        window.addEventListener('beforeunload', _ => {
          clearInterval(__kittenDevelopmentSocketReconnectionAttemptTimeoutId)
          __kittenDevelopmentSocket.removeEventListener('open', socketOpenHandler)
          __kittenDevelopmentSocket.removeEventListener('close', socketCloseHandler)
          __kittenDevelopmentSocket.close()
        }) 
      }

      __kittenConnectToDevelopmentSocket()
    </script>
  `
