import fs from 'node:fs'
import path from 'node:path'
import crypto from 'node:crypto'

import paths from './paths'

import JSDB from '@small-tech/jsdb'
import WebSocket from 'ws'
import htm from 'htm'
import vhtml from 'vhtml'

export default class Globals {
  static initialise() {
    // Set up the global JavaScript Database (JSDB) instance at a directory
    // that is a unique identifier based on the absolute path that the project
    // is in. This should enable all projects to have unique databases.
    const projectIdentifier = process.env.basePath.replace(/\//g, '.').slice(1)
    const databaseDirectory = path.join(paths.KITTEN_DATABASES_DIRECTORY, projectIdentifier)

    // Ensure the database directory exists.
    fs.mkdirSync(databaseDirectory, {recursive: true})

    // Create a symlink to the app’s database directory so we can display that
    // instead of the (likely very long) actual directory name which includes
    // a representation of the app’s absolute path for uniqueness.
    try {
      fs.unlinkSync(paths.KITTEN_CURRENT_DATABASE_SYMLINK_PATH)
    } catch (error) {
      // Ignore error: we don’t care if the symlink doesn’t exist.
    }
    fs.symlinkSync(databaseDirectory, paths.KITTEN_CURRENT_DATABASE_SYMLINK_PATH)

    global.db = JSDB.open(databaseDirectory)

    // Create the kitten table in the database if necessary.
    if (!db.kitten) db.kitten = {
      // This is an app-specific crytographically-secure password that
      // Kitten apps can use to authenticate the person that owns them.
      password: crypto.randomBytes(16).toString('hex')
    }

    // Add global WebSocket reference.
    global.WebSocket = WebSocket

    // Add the html global.
    global.html = htm.bind(vhtml)

    // Add constants for well-known/supported client-side libraries
    // to global scope so they can be referenced in page routes.
    global.HTMX = 'htmx'
    global.HTMX_WEBSOCKET = 'htmx-websocket-extension'
    global.HYPERSCRIPT = 'hyperscript'

    // CSS tagged template.
    //
    // Just does a default merge of the strings and interpolations arrays
    // and wraps them within style tags for now. Not using the HTML tagged template
    // from htm/vhtml as it escapes angular brackets, etc., and also so we don’t
    // have to wrap CSS in a <style></style> tag manually (so we can also keep them
    // in valid external CSS files.)
    global.css = (strings, ...interpolations) => {
      const styles = interpolations.reduce((previous, current, index) => previous + current + strings[index], strings[0])
      return `<style>${styles}</style>`
    }

    // Add a raw function for including raw HTML in templates.
    global.raw = html => vhtml(null, { dangerouslySetInnerHTML: { __html: html }})

    //
    // Authoring helpers for conditionals. Using these functions reads better
    // than using the ternary operator.
    //
    global.is = function (condition) {
      if (condition) {
        return {
          yes: output => {
            return {
              no: _ => {
                return {
                  endIs: _ => output
                }
              },
              endIs: _ => output
            }
          }
        }
      } else {
        return {
          yes: _ => {
            return {
              no: output => {
                return {
                  endIs: _ => output
                }
              },
              endIs: _ => ''
            }
          },
          no: output => {
            return {
              endIs: _ => output
            }
          },
          endIs: _ => ''
        }
      }
    }
    global.isNot = (condition) => is(!condition)
  }
}
