////////////////////////////////////////////////////////////////////////////////
//
// 😸️ Kitten loader.
//
// Associates kitten-specific file extensions with JavaScript modules.
//
// Copyright © 2022-present Aral Balkan, Small Technology Foundation.
// Licensed under GNU AGPL version 3.0.
//
// https://kitten.small-web.org
//
////////////////////////////////////////////////////////////////////////////////

console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}
console.profileTime = process.env.PROFILE ? function () { console.time(...arguments) } : () => {}
console.profileTimeEnd = process.env.PROFILE ? function () { console.timeEnd(...arguments) } : () => {}

import path from 'path'
import fsPromises from 'fs/promises'
import { fileURLToPath } from 'url'
import aliases from '../lib/aliases'

const __dirname = fileURLToPath(new URL('.', import.meta.url))

// Return a resolved object from a path.
function resolvedFromPath(importPath) {
  return {
    url: `file://${importPath}`,
    format: 'module',
    shortCircuit: true
  }
}

// TODO: We’re no longer testing using the context object.
// If we don’t introduce any new tests that need this
// functionality, we should remove it from here.
export const context = {
  fsPromises,
  __dirname
}

export const resolve = (async function (specifier, context, nextResolve) {
  const specifierExtension = path.extname(specifier)

  let parentUrl = null
  let parentFilePath = null
  let parentDirectoryPath = null
  let specifierAbsolutePath = null

  if (context.parentURL) {
    parentUrl = new URL(context.parentURL)
    parentFilePath = parentUrl.pathname
    parentDirectoryPath = path.dirname(parentFilePath)
    specifierAbsolutePath = path.resolve(parentDirectoryPath, specifier)
  }

  const isKittenAsset = aliases[specifierExtension]
  let resolved = null

  switch (true) {
    case isKittenAsset:
      if (specifierAbsolutePath === null) {
        console.error('[LOADER] ERROR: Could not resolve kitten asset', specifier, context)
        console.trace()
        process.exit(1)
      }
      resolved = resolvedFromPath(specifierAbsolutePath)
      break

    default:
      // For anything else, let Node do its own magic.
      try {
        resolved = nextResolve(specifier)
      } catch (error) {
        // The default resolver has failed. We cannot recover from this. Panic!
        console.error('[LOADER] ERROR: Could not resolve', specifier, context)
        console.trace()
        process.exit(1)
      }
  }
  
  return resolved
}).bind(context)
