////////////////////////////////////////////////////////////
//
// Kitten main process; command-line interface (CLI)
//
// Copyright ⓒ 2021-present, Aral Balkan
// Small Technology Foundation
//
// License: AGPL version 3.0.
//
////////////////////////////////////////////////////////////

import CLI from '../cli'

console.verbose = process.env.VERBOSE ? function () { console.log(...arguments) } : () => {}

const cli = new CLI()
cli.parse(process.argv)
